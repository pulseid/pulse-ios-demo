# Pulse iOS Demo

This is a simple working demo app that utilizes our iOS SDK. The complete iOS documentation can be found [here](http://support.pulseid.com/sdk/ios/index.html).

### Quickstart 

Once you've cloned this repo, following the instructions below. 

run `pod install`

open PulseDemo.xcworkspace in Xcode

After that you need to setup your platform credentials in `AppDelegate.m` by changing the following placeholders:

```
NSDictionary* config=@{@"key":@"api-key", @"url":@"endpoint"};
```

### How to simulate location in Xcode

In Xcode, we use gpx file 'Melbourne' which is included in the demo.
```
<wpt lat="-37.800582" lon="144.907554">
    <name>Your Location</name>
    <time>2017-10-02T14:55:00Z</time>
</wpt>
```
Replace the lat,lon to the simulated location.

If you want to simulate the location once the App is launched, you can specify it in Scheme (Run->Options->CoreLocation->Default Location).

In order to jump to other locations whilst the App is running, you can click the location icon in Debug Area.

---------

In case you encounter any issues please email: support@pulseid.com