//
//  AppDelegate.h
//  PulseDemo
//
//  Created by EvanTsai on 2017/9/28.
//

#import <UIKit/UIKit.h>
@import UserNotifications;
@import PulseCore;
@interface AppDelegate : UIResponder <UIApplicationDelegate, PulseSDKDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UNUserNotificationCenter *center;

@end

