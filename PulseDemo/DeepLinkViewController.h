//
//  DeepLinkViewController.h
//  PulseDemo
//
//  Created by Pulse on 11/9/18.
//  Copyright © 2018 demo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeepLinkViewController : UIViewController
@property (strong, nonatomic) NSString *deeplinkText;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
