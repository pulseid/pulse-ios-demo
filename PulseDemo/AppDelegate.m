#import "AppDelegate.h"
#import "DeepLinkViewController.h"
@import Foundation;

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSDictionary *config=@{@"key":@"api-key", @"url":@"endpoint"};
    
    [PulseSDK.sharedClient setConfig:config];
    PulseSDK.sharedClient.delegate = self;
    
    if (@available(iOS 10, *)) {
        self.center=[UNUserNotificationCenter currentNotificationCenter];
        self.center.delegate = self;
    } else {
        if ([launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey]) {
            UILocalNotification *notification = [launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
            [PulseSDK.sharedClient loadViewOnNotificationClick:notification startInForeground:YES background:YES];
        }
    }
    [PulseSDK.sharedClient startMonitoring];
    return YES;
}
// MARK: PulseSDK delegate
-(void)didReceiveDeepLinkNotification:(NSString *)notification{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DeepLinkViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"deeplink"];
    vc.deeplinkText = notification;
    [self.window.rootViewController presentViewController:vc animated:YES completion:nil];
}


// MARK: iOS 9 UILocalNotification
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    //app was in background and user clicked on local alert notification to bring app in foreground
    [PulseSDK.sharedClient loadViewOnNotificationClick:notification startInForeground:YES background:YES];
}

// MARK: iOS 10/11 UserNotification
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {
    //app was in background and user clicked on local alert notification to bring app in foreground
    [PulseSDK.sharedClient loadViewOnUNNotificationClick:response.notification startInForeground:YES background:YES];
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    //app was in foreground
    [PulseSDK.sharedClient loadViewOnUNNotificationClick:notification startInForeground:YES background:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
}
- (void)applicationWillResignActive:(UIApplication *)application{
}
- (void)applicationDidEnterBackground:(UIApplication *)application{
}
- (void)applicationWillEnterForeground:(UIApplication *)application{
}

@end

