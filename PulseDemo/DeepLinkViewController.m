//
//  DeepLinkViewController.m
//  PulseDemo
//
//  Created by Pulse on 11/9/18.
//  Copyright © 2018 demo. All rights reserved.
//

#import "DeepLinkViewController.h"

@interface DeepLinkViewController ()

@end

@implementation DeepLinkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.textView.text = self.deeplinkText;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
